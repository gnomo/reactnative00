// Importar las dependecias
import React from 'react';
import {
	View,
	Text,
	StyleSheet
} from 'react-native';
/*Destructuring en import
Antes:import ReactNative from 'react-native';
 <ReactNative.View>
        <ReactNative.Text>
            CARD (EJEMPLO)
        </ReactNative.Text>
    </ReactNative.View>

Despues:import {View, Text} from 'react-native';
*/
// Funcion sin nombre

const Card = () => { //Component Funcional
	// ejecucuion de funcion
	const imageSource = {
		uri: "https://vignette.wikia.nocookie.net/gravityfalls/images/8/82/Mabel%27s_Sweater_Creator_random_gnome.png/revision/latest?cb=20130902003004",
	};
	return ( 
        <View style={styles.container}>
        <Text style={styles.title}>
            CARD (EJEMPLO)!
        </Text>
            <Image source={imageSource}/>
        </View>
    )
};

const styles = StyleSheet.create({
	container: {
		height: 200,
		width: 300,
		backgroundColor: "#F000000",
	},
	title: {
		fontSize: 30
	}
});
export default Card;
